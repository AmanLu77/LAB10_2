#include <BMP.hpp>
#include <iostream>
/*
�������������� �����, ������� ������������� �� ������, � ����������� ������� ����������� �� ������������ ����. � ���������� ����������� ����� �������������� ���������. ��� ��������� ��-�� ������ ����������.
���� ������ ����������� � ���, ����� ��� ��������� ������.
���� �� ���������, ��� ��� ������� � ������������.
*/
int main()
{
    try
    {
        mt::images::BMP test_bmp(200, 100);
        test_bmp.Fill({ 150,0,100 });
        test_bmp.Rotate(acos(-1) / 6);
        test_bmp.Interpolation_nearest_neighbours();
        test_bmp.SaveFile("test.bmp");
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
